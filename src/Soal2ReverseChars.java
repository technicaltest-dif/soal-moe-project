public class Soal2ReverseChars {

    public static void main(String[] args) {
        char[] arr = {'h', 'e', 'l', 'l', 'o'};
        reverseString(arr);
        System.out.println("Output: " + new String(arr));

    }

    private static void reverseString(char[] s) {
        reverseChars(s, 0, s.length - 1);
    }

    private static void reverseChars(char[] s, int start, int end) {
        if (start >= end) return;

        char temp = s[start];
        s[start] = s[end];
        s[end] = temp;
        reverseChars(s, start + 1, end - 1);
    }

}
