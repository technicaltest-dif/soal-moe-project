import java.util.Stack;

public class Soal3BalancedBrackets {

    public static void main(String[] args) {
        String param = "{ it’s (balance {[with)}) }";
        String result = checkBalancedBrackets(param);
        System.out.println("Output: " + result);

        param = "{ it’s balance with }";
        result = checkBalancedBrackets(param);
        System.out.println("Output: " + result);
    }

    private static String checkBalancedBrackets(String str) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == '(' || c == '{' || c == '[') {
                stack.push(c);
            } else if (c == ')' || c == '}' || c == ']') {
                if (stack.isEmpty()) {
                    return "NO";
                }
                char top = stack.pop();
                if ((c == ')' && top != '(') || (c == '}' && top != '{') || (c == ']' && top != '[')) {
                    return "NO";
                }
            }
        }

        if (stack.isEmpty()) {
            return "YES";
        } else {
            return "NO";
        }
    }


}
