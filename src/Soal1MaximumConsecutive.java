public class Soal1MaximumConsecutive {

    public static void main(String[] args) {
        int[] arr = {1,-1,0,1,1,1};
        int maxConsecutiveOnes = findMaxConsecutiveOnes(arr);
        System.out.println("Output: " + maxConsecutiveOnes);

    }

    private static int findMaxConsecutiveOnes(int[] params) {
        int maxCount = 0;
        int count = 0;
        for (int num : params) {
            validateBinaryNumber(num);
            if (num == 1) {
                count++;
            } else {
                maxCount = Math.max(maxCount, count);
                count = 0;
            }
        }
        return Math.max(maxCount, count);
    }

    private static void validateBinaryNumber(int num) {
        if (num != 0 && num != 1) {
            throw new IllegalArgumentException("Input array must only contain binary numbers (0 and 1)");
        }

        if (num < 0) {
            throw new IllegalArgumentException("Input array must positive numbers");
        }
    }

}
